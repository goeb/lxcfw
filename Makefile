CNFDIR ?= /etc/lxcfw
PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin
SHRDIR ?= $(PREFIX)/share/lxcfw
DOCDIR ?= $(PREFIX)/share/doc/lxcfw
MANDIR ?= $(PREFIX)/share/man
SRVDIR ?= $(PREFIX)/lib/systemd/system
TMPDIR ?= $(PREFIX)/lib/tmpfiles.d

doc: doc/lxcfw.8 doc/lxcfw-getconfig.8 doc/lxcfw-hook.8

clean:

	rm -f doc/lxcfw.8
	rm -f doc/lxcfw-getconfig.8
	rm -f doc/lxcfw-hook.8

install: doc

	# Executables etc.:

	install -d                                        $(DESTDIR)$(BINDIR)
	install -d                                        $(DESTDIR)$(SHRDIR)
	install -d                                        $(DESTDIR)$(SHRDIR)/dnsmasq

	install -m 755 lxcfw                              $(DESTDIR)$(BINDIR)
	install -m 755 lxc/lxcfw-getconfig                $(DESTDIR)$(BINDIR)
	install -m 755 lxc/lxcfw-hook                     $(DESTDIR)$(BINDIR)
	install -m 755 dnsmasq/lxcfw-dhcp-handler         $(DESTDIR)$(SHRDIR)/dnsmasq

	install -m 644 share/functions                    $(DESTDIR)$(SHRDIR)

	# Documentation:

	install -d                                        $(DESTDIR)$(DOCDIR)
	install -d                                        $(DESTDIR)$(MANDIR)/man8

	install -m 644 README                             $(DESTDIR)$(DOCDIR)
	install -m 644 dnsmasq/README                     $(DESTDIR)$(DOCDIR)/README.dnsmasq
	install -m 644 lxc/README                         $(DESTDIR)$(DOCDIR)/README.lxc
	install -m 644 service/README                     $(DESTDIR)$(DOCDIR)/README.service
	install -m 644 doc/lxcfw.8                        $(DESTDIR)$(MANDIR)/man8
	install -m 644 doc/lxcfw-getconfig.8              $(DESTDIR)$(MANDIR)/man8
	install -m 644 doc/lxcfw-hook.8                   $(DESTDIR)$(MANDIR)/man8

	# Service files etc.:

	install -d                                        $(DESTDIR)$(SRVDIR)
	install -d                                        $(DESTDIR)$(TMPDIR)

	install -m 644 dnsmasq/lxcfw-dhcp-handler.service $(DESTDIR)$(SRVDIR)
	install -m 644 lxc/lxcfw-hook.service             $(DESTDIR)$(SRVDIR)
	install -m 644 service/lxcfw-init.service         $(DESTDIR)$(SRVDIR)
	install -m 644 service/lxcfw.service              $(DESTDIR)$(SRVDIR)

	install -m 644 dnsmasq/lxcfw-dhcp-fifo.conf       $(DESTDIR)$(TMPDIR)

	# Example configuration:

	cp -avx config                                    $(DESTDIR)$(DOCDIR)

	# Base configuration:

	install -d                                        $(DESTDIR)$(CNFDIR)
	install -d                                        $(DESTDIR)$(CNFDIR)/rules.d

	awk '/^#####/{X=1}X<1{print}' config/lxcfw.conf | head -n -1 \
		>$(DESTDIR)$(CNFDIR)/lxcfw.conf

	# Fix paths:

	sed -i "s@/usr/local@$(PREFIX)@g"                             \
		$(DESTDIR)$(BINDIR)/lxcfw                                  \
		$(DESTDIR)$(BINDIR)/lxcfw-hook                             \
		$(DESTDIR)$(DOCDIR)/config/rules.d/500_router-traffic      \
		$(DESTDIR)$(DOCDIR)/config/rules.d/500_multicast-broadcast \
		$(DESTDIR)$(DOCDIR)/README.lxc                             \
		$(DESTDIR)$(MANDIR)/man8/lxcfw.8                           \
		$(DESTDIR)$(MANDIR)/man8/lxcfw-hook.8                      \
		$(DESTDIR)$(SHRDIR)/dnsmasq/lxcfw-dhcp-handler             \
		$(DESTDIR)$(SRVDIR)/lxcfw-dhcp-handler.service

uninstall:

	rm -f  $(DESTDIR)$(BINDIR)/lxcfw
	rm -f  $(DESTDIR)$(BINDIR)/lxcfw-getconfig
	rm -f  $(DESTDIR)$(BINDIR)/lxcfw-hook
	rm -f  $(DESTDIR)$(MANDIR)/man8/lxcfw.8
	rm -f  $(DESTDIR)$(MANDIR)/man8/lxcfw-getconfig.8
	rm -f  $(DESTDIR)$(MANDIR)/man8/lxcfw-hook.8
	rm -f  $(DESTDIR)$(SRVDIR)/lxcfw-dhcp-handler.service
	rm -f  $(DESTDIR)$(SRVDIR)/lxcfw-hook.service
	rm -f  $(DESTDIR)$(SRVDIR)/lxcfw-init.service
	rm -f  $(DESTDIR)$(SRVDIR)/lxcfw.service
	rm -f  $(DESTDIR)$(TMPDIR)/lxcfw-dhcp-fifo.conf

	rm -rf $(DESTDIR)$(SHRDIR)
	rm -rf $(DESTDIR)$(DOCDIR)
	rm -rf $(DESTDIR)$(CNFDIR)

dnsmasq-install:

	install -d                                          $(DESTDIR)$(SHRDIR)/dnsmasq
	install -d                                          $(DESTDIR)$(SRVDIR)

	install -m 755 dnsmasq/lxcfw-dnsmasq-script         $(DESTDIR)$(SHRDIR)/dnsmasq

	install -m 644 dnsmasq/lxcfw-dnsmasq-expire.service $(DESTDIR)$(SRVDIR)
	install -m 644 dnsmasq/lxcfw-dnsmasq-expire.timer   $(DESTDIR)$(SRVDIR)

dnsmasq-uninstall:

	rm -f $(DESTDIR)$(SHRDIR)/dnsmasq/lxcfw-dnsmasq-script

	rmdir --ignore-fail-on-non-empty $(DESTDIR)$(SHRDIR)/dnsmasq
	rmdir --ignore-fail-on-non-empty $(DESTDIR)$(SHRDIR)

	rm -f $(DESTDIR)$(SRVDIR)/lxcfw-dnsmasq-expire.service
	rm -f $(DESTDIR)$(SRVDIR)/lxcfw-dnsmasq-expire.timer

doc/lxcfw.8: doc/lxcfw.8.rst
	rst2man doc/lxcfw.8.rst >doc/lxcfw.8

doc/lxcfw-getconfig.8: doc/lxcfw-getconfig.8.rst
	rst2man doc/lxcfw-getconfig.8.rst >doc/lxcfw-getconfig.8

doc/lxcfw-hook.8: doc/lxcfw-hook.8.rst
	rst2man doc/lxcfw-hook.8.rst >doc/lxcfw-hook.8

.PHONY: clean dnsmasq-install dnsmasq-uninstall doc install uninstall

# :indentSize=3:tabSize=3:noTabs=false:mode=makefile:maxLineLen=87: