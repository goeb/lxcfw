=======================================================================================
lxcfw
=======================================================================================

---------------------------------------------------------------------------------------
Yet another firewall script.
---------------------------------------------------------------------------------------

:Author:         Stefan Göbel <lxcfw at subtype dot de>
:Date:           2019/03/14
:Version:        0.1.3
:Manual section: 8
:Manual group:   Admin Commands

SYNOPSIS
=======================================================================================

**lxcfw** *<command>* *[options]*

DESCRIPTION
=======================================================================================

**lxcfw** is a simple *Bash* script to manage (load, save etc.) netfilter rules. It
uses the `nft` command instead of `iptables`, see *nft*\ (8) for details.

Note: All commands work atomically, even when reloading the rules from the scripts in
the `rules.d` directory. This is done by applying the rules in a new temporary network
namespace first, and then copying the complete rule set to the original namespace. This
means the kernel has to support network namespaces!

COMMANDS AND OPTIONS
=======================================================================================

*drop*
---------------------------------------------------------------------------------------

Flush the rule set, create a new `filter` table and drop all input, output and forward
traffic.

--drop-routing             Also drop pre- and postrouting traffic.

*flush*
---------------------------------------------------------------------------------------

Flush the rule set, i.e. delete all rules/tables/chains/sets etc. Since there is no
default policy with `nft`, this will allow all traffic.

--nat                      After flushing the rule set, create a new `nat` table and
                           enable masquerading.

*list*
---------------------------------------------------------------------------------------

List the currently active rule set. Suitable to create an `nft` script. Addresses and
ports will be shown by their numeric values (by using `nft -nn`). This is the default
command if none is specified.

--directory <DIRECTORY>    For `--preview`, the `rules.d` directory to use instead of
                           the default. Defaults to the one set in the configuration,
                           or `/etc/lxcfw/rules.d` if unset.

--file <FILE>              For `--preview`, load rules from this file instead of the
                           `rules.d` scripts.

--keep-empty               Do not remove empty tables from the output (only useful with
                           `--no-chains`).

--no-chains                Omit all chains, i.e. print tables, sets, maps etc. only.

--no-elements              Do not include elements of sets and maps in the output.

--no-flush                 Do not add the `flush ruleset` line when listing the rule
                           set.

--preview                  Do not list the currently loaded rules, list the rules
                           defined in the `rules.d` scripts instead. See also the
                           `--directory` and `--file` options.

--restore-sets             With `--preview`, also include the currently loaded sets,
                           maps etc.

*load*
---------------------------------------------------------------------------------------

Flush the rules and restore the rule set from the configured file. The file must be
suitable to be loaded by the `nft` command, and contain all the rules required. *load*
will not process the scripts in the `rules.d` directory, use *reload* for this purpose.

--file <FILE>              The file to load. If not set the one set in the config will
                           be loaded, or `/etc/lxcfw/rules.nft` if unset.

--keep-empty               Do not remove empty tables from the rules (only useful with
                           `--no-chains`).

--no-chains                Do not load actual netfilter rules, only load sets and maps.
                           Tables would be empty and will not be created by default,
                           unless `--keep-empty` is specified.

--no-elements              Do not add elements of sets and maps even if included in the
                           saved rules.

--no-flush                 Do not flush the current rules before loading the rules from
                           the file.

--restore-sets             If set, elements of currently existing sets and maps will be
                           restored after the rules have been loaded.

*reload*
---------------------------------------------------------------------------------------

Flush and regenerate the rule set from the scripts in the `rules.d` directory. To load
rules from a single file in `nft` format, use the *load* command.

--directory <DIRECTORY>    The `rules.d` directory to use instead of the default.
                           Defaults to the one set in the configuration, or
                           `/etc/lxcfw/rules.d` if unset.

--keep-empty               Do not remove empty tables from the rules (only useful with
                           `--no-chains`).

--no-chains                Do not load actual netfilter rules, only load sets and maps.
                           Tables would be empty and will not be created by default,
                           unless `--keep-empty` is specified.

--no-elements              If set, elements of maps and sets set by `rules.d` scripts
                           will be ignored.

--no-flush                 Do not flush the current rules before loading the new rules.

--restore-sets             If set, elements of currently existing sets and maps will be
                           restored after reloading the rules.

*save*
---------------------------------------------------------------------------------------

Save the currently loaded rule set to the configured file.

--directory <DIRECTORY>    For `--preview`, the `rules.d` directory to use instead of
                           the default. Defaults to the one set in the configuration,
                           or `/etc/lxcfw/rules.d` if unset.

--file <FILE>              For `--preview`, load rules from this file instead of the
                           `rules.d` scripts. *NOT* the file to save the rules to, see
                           `--output` for that.

--keep-empty               Do not remove empty tables from the rules (only useful with
                           `--no-chains`).

--no-chains                Do not save actual netfilter rules, only save sets and maps.
                           Tables would be empty and will not be included by default,
                           unless `--keep-empty` is specified.

--no-elements              Do not save the elements of existing sets and maps.

--no-flush                 Do not add the `flush ruleset` line when saving the rule
                           set.

--output <FILE>            Overwrite the output file set in the configuration. Defaults
                           to `/etc/lxcfw/rules.nft` if unset.

--preview                  Do not save the currently loaded rule set, use the one in
                           `rules.d` or a specified file. See `--directory` and
                           `--file`.

--restore-sets             With `--preview`, also include the currently loaded sets,
                           maps etc.

CONFIGURATION
=======================================================================================

Configuration is located in `/etc/lxcfw`, with the main configuration file being
`/etc/lxcfw/lxcfw.conf`. This can not be changed (unless you want to modify the
**lxcfw** script).

The default rules directory is `/etc/lxcfw/rules.d`. This, as well as other defaults,
may be changed in the configuration file.

The *reload* command will source all executable files in the `rules.d` directory, in
alphabetical order (this may depend on the locale settings).

The `rules.d` scripts must be Bash scripts, as mentioned above they will be sourced,
not executed. These scripts must set up the netfilter rules, by calling `nft` as
appropriate.

*NOTE*: The scripts must call `nft` by this name only, and not by its full path! The
reason for that is that the **lxcfw** script defines a function by this name (thus
overriding the command call if only called as `nft`), that wraps the actual command to
load the rules in the temporary network namespace. If for some reason the real binary
should be called, use its full path (e.g. `/usr/bin/nft`).

If installed, an example configuration file and rule scripts are available in
`/usr/local/share/lxcfw/config/` (or similar). Otherwise check the source distribution.

SEE ALSO
=======================================================================================

*lxcfw-hook*\ (8), *nft*\ (8)

LICENSE
=======================================================================================

Copyright © 2017-2019 Stefan Göbel < lxcfw ʇɐ subtype ˙ de >.

**lxcfw** is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

**lxcfw** is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with **lxcfw**.
If not, see <http://www.gnu.org/licenses/>.

.. :indentSize=3:tabSize=3:noTabs=true:mode=rest:maxLineLen=87: