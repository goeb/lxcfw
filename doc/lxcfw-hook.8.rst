=======================================================================================
lxcfw-hook
=======================================================================================

---------------------------------------------------------------------------------------
Add or remove LXC container data to netfilter sets on container start/stop.
---------------------------------------------------------------------------------------

:Author:         Stefan Göbel <lxcfw at subtype dot de>
:Date:           2019/03/14
:Version:        0.1.3
:Manual section: 8
:Manual group:   Admin Commands

SYNOPSIS
=======================================================================================

When this script is called manually (as opposed to being called as an LXC hook), the
syntax is:

   **lxcfw-hook** *<container>* *<action>* *[values]*

The first parameter must be either the container name to process only this one
container, or one of `--all`, `--active` (running and frozen), `--running`, `--frozen`
or `--stopped`, in which case all containers or those with the specified state will be
affected.

The second parameter must be `pre-start` or `post-stop` (like the LXC hooks), or, just
for convenience, `start` or `stop`, which do the same (do *NOT* use these LXC hooks,
though, it's really just for convenience when calling **lxcfw-hook** manually).

Any following parameters may define set element values, in the same format as used in
the group names, e.g. to set the first interface's IP use `@ip0=<ip>`. If a values is
defined like this, it will not be looked up in the container configuration. Note that
`@snat<number>` must not be used before `@ip<number>` has been set! For IPs, the
special value `@current` may be used to set it to the IP provided by the lxc-info
output (only use this for running containers, it will always be the first IP listed in
the output). If more than one container is processed settings must be prefixed by
`<container>/`, otherwise they will be applied to all containers being processed.

When called automatically as an LXC hook, **lxcfw-hook** will be called as follows:

   **lxcfw-hook** *<container name>* `lxc` *<hook type>*

It works as both the pre-start and post-stop hook, the container configuration should
look like this:

   lxc.hook.pre-start = /usr/local/bin/lxcfw-hook
   lxc.hook.post-stop = /usr/local/bin/lxcfw-hook
   lxc.hook.version   = 0

It is recommended to set `lxc.hook.version` to `0` for any container using the hook,
although at the moment it works without explicitely setting this.

DESCRIPTION
=======================================================================================

**lxcfw-hook** may be used as an LXC pre-start and post-stop hook to automatically add
data like the container IP or port numbers to netfilter (`nft`) rule sets or maps. The
container has to be configured appropriately, and the netfilter rules and sets/maps
have to be created manually before this script is called.

To add data to a role set, the container has to be added to a group named after the
set. For a set named `example`, to add a container's IP to it, the container has to be
added to the group called `role-example`:

   lxc.group = role-example

The data to add to the set can be configured if required, if not set, it defaults to
the IPv4 address of the first interface of the container (`lxc.net.0.ipv4.address`,
with the netmask removed). The general group format recognized by **lxcfw-hook** is:

   lxc.group = role-<role name>=<data>

The <data> may be a constant, e.g. an IP address or a port number. It may be
`@ip<number>`, which will be replaced by the IP of the interface with the specified
index, or `@gw<number>`, replaced by its gateway, or `@hw<number>`, replaced by the
MAC address of the specified interface.

To concatenate values, `+` may be used. To add values to a map, `:` separates keys and
values.

The default table is called `filter` (ip only). To populate sets in other tables, they
have to be spcified in the format

   lxc.group = role-<protocol>.<table>.<role name>=<data>

Some examples:

* Add the first IPv4 address to the http-client set:

   `lxc.group = role-http-client`

* Same as above:

   `lxc.group = role-http-client=@ip0`

* Add the IPv4 address of the second interface to the http-client set:

   `lxc.group = role-http-client=@ip1`

* Add { first interface's IP . port 8080 } to the http-server set:

   `lxc.group = role-http-server=@ip0+8080`

* Add { first IP : <NATed IP> } to the ip nat table's snat map (see below):

   `lxc.group = role-ip.nat.snat=@ip0:@snat0`

The special value `@snat<number>` will be replaced depending on the configuration in
`/etc/lxcfw/lxcfw.conf`. If `$snat_min` and `$snat_max` are set, to a value in the
specified interval (depending on the `@ip<number>` value). If only one of these is set,
it will be set to this value. If none of these are set, the role will be ignored. The
NATed IP address will be added to the `$snat_iface` interface with the `$snat_nmask`
netmask. If these are not set, no address will be added to any interface (sets or maps
will still be modified if applicable). Note that an address will never be removed from
the interface (since it may still be in use even after a container is shut down).

If a specified set or map does not exist, or if a requested value is not set in the
container configuration (or it is empty), no element will be added and processing
continues with the next group (if there is any).

Note that the set/map elements will be added one at a time during container group
processing.

CONFIGURATION
=======================================================================================

Configuration values are read from `/etc/lxcfw/lxcfw.conf` (this can not be changed,
unless you want to modify the **lxcfw-hook** script), and of course from the container
configuration, as described above.

If installed, an example configuration file and rule scripts are available in
`/usr/local/share/lxcfw/config/` (or similar). Otherwise check the source distribution.

SEE ALSO
=======================================================================================

*lxc.container.conf*\ (5), *lxcfw*\ (8), *nft*\ (8)

LICENSE
=======================================================================================

**lxcfw-hook** is part of **lxcfw**.

Copyright © 2017-2019 Stefan Göbel < lxcfw ʇɐ subtype ˙ de >.

**lxcfw** is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, either
version 3 of the License, or (at your option) any later version.

**lxcfw** is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with **lxcfw**.
If not, see <http://www.gnu.org/licenses/>.

.. :indentSize=3:tabSize=3:noTabs=true:mode=rest:maxLineLen=87: