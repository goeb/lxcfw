# Maintainer: Stefan Göbel < lxcfw ʇɐ subtype ˙ de >

pkgbase='lxcfw'
pkgname=( "$pkgbase" "$pkgbase-dnsmasq" )

pkgver='0.1.3'
pkgrel='1'
arch=( 'any' )

url='https://gitlab.com/goeb/lxcfw/'
license=( 'GPL3' )

makedepends=( 'python-docutils' )

_pkgbuild_dir=${_pkgbuild_dir:-$PWD}
_source_files=(
   'config'
   'dnsmasq'
   'doc'
   'lxc'
   'service'
   'share'
   'LICENSE'
   'lxcfw'
   'Makefile'
   'PKGBUILD'
   'README'
)

prepare() {

   local _dest="$srcdir/$pkgbase"
   local _file=''

   mkdir -p "$_dest"

   for _file in "${_source_files[@]}" ; do
      cp -avx "$_pkgbuild_dir/$_file" "$_dest/"
   done

   cd "$_dest"
   make clean

}

build() {

   cd "$srcdir/$pkgbase"
   make

}

package_lxcfw() {

   backup=( 'etc/lxcfw/lxcfw.conf' )
   pkgdesc='Firewall script using nft, with some LXC specific features.'
   depends=( 'nftables' )
   optdepends=(
      'lxc: For the containers.'
      'python-lxc: Required for the lxcfw-hook script.'
   )

   cd "$srcdir/$pkgbase"
   make install DESTDIR="$pkgdir" PREFIX='/usr'

}

package_lxcfw-dnsmasq() {

   pkgdesc='dnsmasq support scripts for lxcfw.'
   depends=( 'dnsmasq' )

   cd "$srcdir/$pkgbase"
   make dnsmasq-install DESTDIR="$pkgdir" PREFIX='/usr'

}

#:indentSize=3:tabSize=3:noTabs=true:mode=shellscript:maxLineLen=78: